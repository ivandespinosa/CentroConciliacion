﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CentroConciliacion.Models
{
    public class Departament
    {
        [Key]
        public int DepartamentId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Index("Departament_Name_Index", IsUnique = true)]
        [StringLength(30, ErrorMessage = "El campo {0} maximo {1} y mínimo {2} characteres", MinimumLength = 1)]
        public string Nombre { get; set; }
    }
}