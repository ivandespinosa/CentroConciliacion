﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentroConciliacion.Models
{
    public class CentroConciliacionContext : DbContext 
    {
        //Constructor -> conecta con el DefaultConnection
        public CentroConciliacionContext() : base("DefaultConnection")
        {

        }

        //Cierra la conexión a la base de datos
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<CentroConciliacion.Models.Departament> Departaments { get; set; }
    }
}
