﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CentroConciliacion.Startup))]
namespace CentroConciliacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
